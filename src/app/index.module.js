(function() {
  'use strict';

  angular
    .module('app', ['ngRoute', 'mgcrea.ngStrap', 'toastr', 'btford.socket-io']);

})();
