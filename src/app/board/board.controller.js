(function() {
  'use strict';

  angular
    .module('app')
    .controller('BoardController', BoardController);

  /** @ngInject */
  function BoardController($scope, $location, authService, socket, toastr) {
    var vm = this;
    vm.input = '';
    vm.leadersList = [];
    vm.playersList = [];
    vm.fails = 0;
    vm.word = [];
    vm.usedLetters = '';

    vm.check = function (text) {
      if (text.length === 1 && vm.usedLetters.indexOf(text) !== -1 ) {
        toastr.info('Letter already used.');
      } else if (text.length === 1) {
        socket.emit('letter-guess', text);
      } else if (text.length > 1) {
        socket.emit('word-guess', text);
      }
      vm.input = '';
    };

    toastr.success('Game started! Have fun.');
    socket.emit('leaders-request');
    socket.emit('next-word-request');

    $scope.$on('socket:guess-fail', function (ev, data) {
      if (data.gameOver) {
        authService.playerState('gameover');
        $location.path('/gameover');
      }
      vm.fails = data.fails;
      vm.usedLetters = data.usedLetters;
    });

    $scope.$on('socket:word', function (ev, data) {
      vm.word = [];
      vm.usedLetters = '';
      vm.fails = 0;
      for (var i = 0; i < data; i++) {
        vm.word.push(' ');
      }
    });

    $scope.$on('socket:letter-guessed', function (ev, data) {
      vm.usedLetters = data.usedLetters;
      data.indexes.forEach(function (item) {
        vm.word[item] = data.letter;
      });
      authService.playerPoints(data.points);
    });

    $scope.$on('socket:word-guessed', function (ev, data) {
      toastr.success('Word guessed. Good job, keep goin!');
      socket.emit('next-word-request');
      authService.playerPoints(data.points);
    });

    $scope.$on('socket:scores-change', function (ev, data) {
      vm.leadersList = data;
    });

    $scope.$on('socket:players-change', function (ev, data) {
      vm.playersList = data;
    });
  }
})();
