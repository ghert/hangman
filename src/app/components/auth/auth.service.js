(function() {
  'use strict';

  angular
    .module('app')
    .factory('authService', function () {
        var data = {
            isLoggedIn: false,
            isInGame: false,
            playerPoints: 0
        };
        return {
            loginState: function (state) {
              if (state) {
                data.isLoggedIn = state;
              } else {
                return data.isLoggedIn;
              }
            },
            playerState: function (state) {
              if (state) {
                data.isInGame = state;
              } else {
                return data.isInGame;
              }
            },
            playerPoints: function (points) {
              if (points) {
                data.playerPoints = points;
              } else {
                return data.playerPoints;
              }
            }
        };
    });

})();
