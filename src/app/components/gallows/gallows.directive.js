(function() {
  'use strict';

  angular
    .module('app')
    .directive('gallows', gallows);

  /** @ngInject */
  function gallows() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/gallows/gallows.html',
      scope: {
          fails: '='
      },
      controller: GallowsController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function GallowsController(moment) {
      var vm = this;
    }
  }

})();
