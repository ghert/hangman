(function() {
  'use strict';

  angular
    .module('app')
    .directive('leaders', leaders);

  /** @ngInject */
  function leaders() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/leaders/leaders.html',
      scope: {
        list: '='
      },
      controller: leadersController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function leadersController(moment) {
      var vm = this;
    }
  }

})();
