(function() {
  'use strict';

  angular
    .module('app')
    .directive('word', word);

  /** @ngInject */
  function word() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/word/word.html',
      scope: {
          word: '='
      },
      controller: WordController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function WordController(moment) {
      var vm = this;
    }
  }

})();
