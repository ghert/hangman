(function() {
  'use strict';

  angular
    .module('app')
    .directive('players', players);

  /** @ngInject */
  function players() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/players/players.html',
      scope: {
          list: '='
      },
      controller: PlayersController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function PlayersController(moment) {
      var vm = this;
    }
  }

})();
