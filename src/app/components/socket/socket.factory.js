(function() {
  'use strict';

  angular
    .module('app')
    .factory('socket', ['socketFactory', '$rootScope', function (socketFactory, $rootScope) {
      var myIoSocket = io.connect('http://localhost:3456');

      var mySocket = socketFactory({
        ioSocket: myIoSocket
      });

      mySocket.forward([
        'word',
        'guess-fail',
        'word-guessed',
        'letter-guessed',
        'scores-change',
        'players-change'
      ], $rootScope);

      return mySocket;
    }]);

}());
