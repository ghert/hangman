(function() {
  'use strict';

  angular
    .module('app')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'loginCtrl'
      })
      .when('/gameover', {
        templateUrl: 'app/game-over/game-over.html',
        controller: 'GameOverController',
        controllerAs: 'gameOverCtrl'
      })
      .when('/board', {
        templateUrl: 'app/board/board.html',
        controller: 'BoardController',
        controllerAs: 'boardCtrl'
      })
      .otherwise({
        redirectTo: '/login'
      });
  }

})();
