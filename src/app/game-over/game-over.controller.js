(function() {
  'use strict';

  angular
    .module('app')
    .controller('GameOverController', GameOverController);

  /** @ngInject */
  function GameOverController(socket, $location, authService) {
    var vm = this;
    vm.points = authService.playerPoints();
    vm.again = function () {
      authService.playerPoints(0);
      authService.playerState('board');
      $location.path('/board');
    };
  }
})();
