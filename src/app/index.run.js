(function() {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, authService, $location) {
    $rootScope.$on('$locationChangeStart', function (event, next) {
      if (!authService.loginState()) {
        $location.path('/login');
      } else if (authService.playerState() === 'board') {
        $location.path('/board');
      } else if (authService.playerState() === 'gameover') {
        $location.path('/gameover');
      }
    });
  };
})();
