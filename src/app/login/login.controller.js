(function() {
  'use strict';

  angular
    .module('app')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($location, socket, toastr, authService) {
    var vm = this;
    vm.login = function (nick) {
      if (typeof nick !== 'undefined' && nick.length) {
        socket.emit('nick-change', nick)
        authService.loginState(true);
        authService.playerState('board');
        $location.path('/board');
      } else {
        toastr.error('Please fill the nickname input.')
      }
    }
  }
})();
