var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var playersManager = require('./players-manager')(io);
var config = require('./config.json');

io.on('connection', function(socket){
  socket.on('nick-change', function(nick){
    if (playersManager.getList().length < 8) {
      playersManager.add(nick, socket);
    } else {
      socket.emit('max-players');
    }
  });
  socket.on('disconnect', function () {
    playersManager.remove(socket);
    io.emit('players-change', playersManager.getList());
  });
});

server.listen(config.port);
