var config = require('./config.json');
var scores = require('./scores')(config.db);

function PlayerConnection(nick, socket) {
  this.nick = nick;
  this.socket = socket;
  this.points = 0;
  this.usedLetters = '';
  this.guessedLetters = '';
  this.fails = 0;
  this.activeWord = '';
}

PlayerConnection.prototype.checkLetter = function (data) {
  var indexes = [];
  this.activeWord.split('').forEach(function (item, index) {
    if (item === data[0]) {
      indexes.push(index);
    }
  });
  this.points += indexes.length;
  if (this.lettersGuessed >= this.activeWord.length - 1 ) {
    this.socket.emit('word-guessed', {
      points: this.points
    });
  } else {
    if (this.usedLetters.indexOf(data[0]) === -1) {
      this.usedLetters += data[0];
    }
    this.socket.emit('letter-guessed', {
      points: this.points,
      indexes: indexes,
      letter: data[0],
      usedLetters: this.usedLetters
    });
  }
  this.lettersGuessed += indexes.length;
}

PlayerConnection.prototype.checkWord = function (data, list, io) {
  if (this.activeWord === data) {
    this.points += (this.activeWord.length - this.lettersGuessed) * 2;
    this.socket.emit('word-guessed', this.activeWord);
    io.emit('players-change', list);
  } else if (this.activeWord.indexOf(data) === -1) {
    this.punish(data);
  }
};

PlayerConnection.prototype.setNewWord = function (word) {
  this.activeWord = word;
  this.lettersGuessed = 0;
  this.fails = 0;
  this.usedLetters = '';
  this.socket.emit('word', word.length);
};

PlayerConnection.prototype.punish = function (data) {
    this.fails += 1;
    if (this.usedLetters.indexOf(data[0]) === -1) {
      this.usedLetters += data[0];
    }
    this.socket.emit('guess-fail', {
      fails: this.fails,
      gameOver: (this.fails > 5),
      usedLetters: this.usedLetters
    });
    if (this.fails > 5) {
      scores.add(this.nick, this.points);
      this.points = 0;
    }
}

PlayerConnection.prototype.bindEvent = function (name, cb) {
  this.socket.on(name, cb);
};

module.exports = PlayerConnection;
