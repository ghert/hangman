var wordsList = require('./words.json');

module.exports = {
  list: wordsList,
  random: function () {
    var i = Math.floor(Math.random() * (this.list.length - 1));
    return this.list[i];
  }
};
