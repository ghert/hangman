var Firebase = require('firebase');

function scores(dbUrl) {
  var root = new Firebase(dbUrl);
  var scoresRef = root.child("scores");

  return {
    add: function (nick, points) {
      scoresRef.push().set({
        nick: nick,
        points: points
      });
    },
    load: function (cb) {
      var scores = [];
      scoresRef.limitToFirst(5).orderByChild('points').once('value', function(snapshot) {
        snapshot.forEach(function (childSnapshot) {
          scores.push(childSnapshot.val());
        });
        cb(scores.reverse());
      });
    }
  }
}

module.exports = scores;
