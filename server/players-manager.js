var PlayerConnection = require('./player-connection');
var words = require('./words');
var config = require('./config.json');
var scores = require('./scores')(config.db);

function playersManager(io) {
  var players = [];
  return {
    add: function (nick, socket) {
      var player = new PlayerConnection(nick, socket, io);
      var self = this;
      players.push(player);

      player.bindEvent('leaders-request', function () {
        scores.load(function (scores) {
          socket.emit('scores-change', scores);
        })
      });

      player.bindEvent('next-word-request', function () {
        var word = words.random();
        player.setNewWord(word);
      });

      player.bindEvent('word-guess', function (data) {
        player.checkWord(data, self.getList(), io);
      });

      player.bindEvent('letter-guess', function (data) {
        if (player.activeWord.indexOf(data[0]) === -1) {
          player.punish(data);
        } else {
          player.checkLetter(data);
          io.emit('players-change', self.getList());
        }
      });
    },
    remove: function (socket) {
      var index = players.indexOf(this.getFromSocket(socket));
      players.splice(index, 1);
    },
    getFromSocket: function (socket) {
      players.forEach(function (item) {
          if (item.socket.id === socket.id) {
            return item;
          }
      });
    },
    getList: function () {
      var list = [];
      players.forEach(function (item) {
        list.push({
          nick: item.nick,
          points: item.points
        });
      });
      return list;
    }
  };
}

module.exports = playersManager;
