# Setup #
```
#!bash

node server/index.js
gulp serve
```

Disable all sync options in browser-sync settings when playing with more players or gulp build and then serve it with http-server.